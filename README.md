# UPA

Project for UPA class

## How to:
* `git clone https://gitlab.com/_lml_/upa.git project`
* `cd project`
* `./gradlew run`

## Requirements:
* JDK 13
* JavaFX
* JDBC

## Personal evidence:
A short summarisation of our project:

There will be two parts. In the first part you will be able to create office 
space consisting of buldings foot print, inner walls, rooms, doors, tables and lights.

Once footprint is created you will be able to add the rest of the items.

The second part will be kind of managing mode, where you will be able to 
assing workspace to a person and upload his/hers picture to the system.
There will be also ability to create querries about office space and employees.
