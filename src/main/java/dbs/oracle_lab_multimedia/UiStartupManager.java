package dbs.oracle_lab_multimedia;

import dbs.oracle_lab_multimedia.controllers.*;
import dbs.oracle_lab_multimedia.utils.ConnectionContainer;
import dbs.oracle_lab_multimedia.utils.SessionContainer;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * Zajištuje zobrazení UI
 */
public class UiStartupManager {
    public UiStartupManager(Stage stage) {
        stage.setWidth(800);
        stage.setHeight(500);
        stage.setResizable(true);
        stage.setTitle("upaProj");

        ConnectionContainer connectionContainer = new ConnectionContainer();
        SessionContainer sessionContainer = new SessionContainer();

        Scene scene = new Scene(new TextArea());

        LoginController login = new LoginController(scene, connectionContainer);
        BuildingController office = new BuildingController(scene, connectionContainer, sessionContainer);
        PhotosController photos = new PhotosController(scene, connectionContainer, sessionContainer);
        OfficesController offices = new OfficesController(scene, connectionContainer, sessionContainer);
        PersonDetailController personDetail = new PersonDetailController(scene, connectionContainer, sessionContainer);
        PersonEditController personEdit = new PersonEditController(scene, connectionContainer, sessionContainer);
        OfficeCreationController officeCreationController = new OfficeCreationController(scene, connectionContainer, sessionContainer);

        login.setControllers(offices);
        office.setControllers(photos, offices);
        photos.setControllers(office, personDetail, personEdit);
        offices.setControllers(office, officeCreationController);
        personDetail.setControllers(photos, personEdit);
        personEdit.setControllers(photos);
        officeCreationController.setControllers(offices, offices);

        scene.setRoot(login.getRoot());
        login.display();
        stage.setScene(scene);
        stage.show();
    }
}
