package dbs.oracle_lab_multimedia.controllers;

import dbs.oracle_lab_multimedia.models.PersonObject;
import dbs.oracle_lab_multimedia.utils.AlertUtil;
import dbs.oracle_lab_multimedia.utils.ConnectionContainer;
import dbs.oracle_lab_multimedia.utils.SessionContainer;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;


public class PersonEditController extends Controller {
    private PersonObject person;
    private Image picture;
    ImageView iv = new ImageView();
    final private SessionContainer sessionContainer;
    private PhotosController photosController;
    private ConnectionContainer connectionContainer;
    private File photoFile;
    private TextField nameText;
    private final DatePicker datePicker;

    public PersonEditController(final Scene scene, final ConnectionContainer connectionContainer,
                                final SessionContainer sessionContainer) {

        this.sessionContainer = sessionContainer;
        this.connectionContainer = connectionContainer;

        GridPane gridPane = new GridPane();
        gridPane.setMinHeight(20);

        gridPane.add(iv, 1, 0);

        gridPane.add(new Text("Name"),0, 1);

        nameText = new TextField();
        gridPane.add(nameText, 1, 1);

        gridPane.add(new Text("date"), 0, 2);

        datePicker = new DatePicker();
        gridPane.add(datePicker, 1, 2);

        gridPane.add(new Text("photo"), 0, 3);

        Text filePath = new Text();
        gridPane.add(filePath, 2, 3);

        Button selectPhoto = new Button("select photo");
        selectPhoto.setOnAction(e -> {FileChooser photoFileChooser = new FileChooser();
            photoFile = photoFileChooser.showOpenDialog(null);
            filePath.setText(photoFile.getAbsolutePath());
            });
        gridPane.add(selectPhoto, 1, 3);

        Button saveButton = new Button("save");
        saveButton.setPrefSize(200, 60);
        gridPane.add(saveButton, 0, 4);
        saveButton.setOnAction(e -> save());

        Button backButton = new Button("back");
        backButton.setPrefSize(200, 60);
        gridPane.add(backButton, 1,4);
        backButton.setOnAction(e -> {
                scene.setRoot(photosController.getRoot());
                photosController.display();
        });

        Button toGrayscaleButton = new Button("Grayscale Image");
        toGrayscaleButton.setPrefSize(200, 60);
        gridPane.add(toGrayscaleButton, 2, 4);
        toGrayscaleButton.setOnAction(e -> {
            try {
                sessionContainer.getPersonObject().convert2Greyscale(connectionContainer.getConnection());
            }
            catch (SQLException ex) {

            }
            display();
        });

        root = gridPane;

    }

    public void setControllers(PhotosController controller) {
        this.photosController = controller;
    }

    @Override
    public void display() {
        loadPerson();
        try {
            loadImageFromDB();
        }
        catch (SQLException e) {
            e.printStackTrace();
            AlertUtil.showDbGeneralError();
        }
    }

    private void loadImageFromDB () throws SQLException {
        try {
            this.picture = new Image(person.getPhoto().getBlobContent().getBinaryStream());
            iv.setImage(this.picture);
        }
        catch (NullPointerException e) {
            // no image
        }
    }

    public void save() {
        person.setName(nameText.getText());

        person.setDate(Date.valueOf(datePicker.getValue()));

        String fileString;
        try {
            fileString = photoFile.getAbsolutePath();
        }
        catch (NullPointerException e) {
            fileString = new String();
            }
        if (!fileString.isEmpty()) {
            try {
                person.update(connectionContainer.getConnection(), fileString);
            } catch (SQLException e) {
                e.printStackTrace();
                AlertUtil.showDbGeneralError();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                person.update(connectionContainer.getConnection());
            } catch (SQLException e) {
                e.printStackTrace();
                AlertUtil.showDbGeneralError();
            }
        }
        display();
    }

    private void loadPerson() {

        this.person = sessionContainer.getPersonObject();
        nameText.setText(person.getName());
        datePicker.setValue(person.getDate().toLocalDate());
    }

}
