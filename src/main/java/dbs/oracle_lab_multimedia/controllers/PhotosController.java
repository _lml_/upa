package dbs.oracle_lab_multimedia.controllers;
import dbs.oracle_lab_multimedia.repositories.OfficeObjectsRepository;
import dbs.oracle_lab_multimedia.utils.ConnectionContainer;
import dbs.oracle_lab_multimedia.utils.SessionContainer;
import dbs.oracle_lab_multimedia.models.OfficeObject;
import dbs.oracle_lab_multimedia.models.PersonObject;
import dbs.oracle_lab_multimedia.utils.AlertUtil;


import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.Pos;
import javafx.stage.FileChooser;
import oracle.ord.im.OrdImage;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.sql.SQLException;
import static java.lang.System.out;

public class PhotosController extends Controller {
    private static int personGenerated = 0;
    private Controller officeController;
    private final ConnectionContainer connectionContainer;
    private final SessionContainer sessionContainer;
    private final ListView<String> listView;
    private PersonDetailController personDetailController;
    private PersonEditController personEditController;
    private List<PersonObject> people;
    private final Scene scene;
    private File searchImage;
    OfficeObjectsRepository repository = new OfficeObjectsRepository();

    public PhotosController(final Scene scene, final ConnectionContainer connectionContainer,
            final SessionContainer sessionContainer) {
        this.connectionContainer = connectionContainer;
        this.sessionContainer = sessionContainer;
        this.scene = scene;
        listView = new ListView<String>();

        final VBox layout = new VBox();
        layout.setSpacing(10);

        final Button button = new Button("Office");
        button.setPrefSize(220, 60);
        button.setOnAction(e -> scene.setRoot(officeController.getRoot()));
        layout.getChildren().add(button);

        final Button loadPeopleButton = new Button("Load people");
        loadPeopleButton.setPrefSize(220, 60);
        loadPeopleButton.setOnAction(e -> loadPeople());
        layout.getChildren().add(loadPeopleButton);

        final Button addPersonButton = new Button("Add person");
        addPersonButton.setPrefSize(220, 60);
        addPersonButton.setOnAction(e -> addPerson());
        layout.getChildren().add(addPersonButton);

        final Button deletePersonButton = new Button("Delete person");
        deletePersonButton.setPrefSize(220, 60);
        deletePersonButton.setOnAction(e -> deletePerson());
        layout.getChildren().add(deletePersonButton);

        final Button searchByImageButton = new Button("Search by Image");
        final Button selectImageButton = new Button("select image");
        selectImageButton.setOnAction(e -> {
            FileChooser photoFileChooser = new FileChooser();
            searchImage = photoFileChooser.showOpenDialog(null);
            if (searchImage != null) {
                selectImageButton.setText("selected");
            }
        });
        searchByImageButton.setOnAction(e -> {
            if (searchImage != null) {
                selectImageButton.setText("select image");
                findByPhoto();
            }
        });
        HBox imageSearchHBox = new HBox();
        imageSearchHBox.getChildren().addAll(selectImageButton, searchByImageButton);
        layout.getChildren().add(imageSearchHBox);


        listView.setOnMouseClicked(e -> {
            if(e.getClickCount() == 2 && listView.getSelectionModel().getSelectedItem() != null){
                PersonObject person = people.get(listView.getSelectionModel().getSelectedIndex());
                sessionContainer.setPersonObject(person);
                scene.setRoot(personDetailController.getRoot());
                personDetailController.display();
            }
        });

        BorderPane mainLayout = new BorderPane();
        mainLayout.setLeft(layout);
        mainLayout.setCenter(listView);

        root = mainLayout;
    }

    private void findByPhoto() {
        PersonObject imagePerson = new PersonObject();
        imagePerson.setName("Select name");
        imagePerson.setDate("1994-12-14");
        imagePerson.assignOffice(sessionContainer.getOfficeObject());

        try {
            imagePerson.update(connectionContainer.getConnection(), searchImage.getAbsolutePath());
        } catch (SQLException e) {
            e.printStackTrace();
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        try {
            sessionContainer.setPersonObject(imagePerson.getPersonByPhoto(connectionContainer.getConnection()));
            imagePerson.delete(connectionContainer.getConnection());
            personDetailController.display();
            scene.setRoot(personDetailController.getRoot());
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
    public void setControllers(final BuildingController officeController,
                               final PersonDetailController personDetailController,
                               final PersonEditController personEditController) {
        this.officeController = officeController;
        this.personDetailController = personDetailController;
        this.personEditController = personEditController;
    }

    public void loadPeople() {
        people = repository.getPeople(connectionContainer.getConnection(), sessionContainer.getOfficeObject());
        final ObservableList<String> items = FXCollections.observableArrayList();
        for (final PersonObject o : people) {
            items.add(o.getName());
        }
        listView.setItems(items);
    }

    private void addPerson(){
        PersonObject newPerson = new PersonObject();
        newPerson.setName("imagePerson");
        newPerson.setDate("1994-12-14");
        newPerson.assignOffice(sessionContainer.getOfficeObject());
        newPerson.setPhoto(new OrdImage());
        sessionContainer.setPersonObject(newPerson);
        personEditController.display();
        scene.setRoot(personEditController.getRoot());
    }

    private void deletePerson() {
        int selected = listView.getSelectionModel().getSelectedIndex();
        System.out.println("selected index: " + selected);
        try {
            people.get(selected).delete(connectionContainer.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
            AlertUtil.showDbGeneralError();
        }
        loadPeople();
    }

    @Override
    public void display(){
        loadPeople();
    }
}
