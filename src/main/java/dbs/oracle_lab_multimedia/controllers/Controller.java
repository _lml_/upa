package dbs.oracle_lab_multimedia.controllers;

import javafx.scene.Parent;

public class Controller {
    Parent root;

    /**
     * Vrati korenove view kontroleru.
     *
     * Kontrolery se navzajem prepinaji pridanim tohoto view jako korene
     * sceny. Kdyz kontroler zobrazuje view jineho, mel by zavolat
     * take jeho display().
     */
    public Parent getRoot() {
        return root;
    }

    /**
     * Vola se kdyz je view kontroleru zobrazeno uzivateli.
     *
     * Slouzi na obnoveni zobrazovanych informaci.
     */
    public void display(){

    }
}
