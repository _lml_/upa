package dbs.oracle_lab_multimedia.controllers;

import dbs.oracle_lab_multimedia.repositories.OfficeObjectsRepository;
import dbs.oracle_lab_multimedia.utils.ConnectionContainer;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import java.sql.Connection;

public class LoginController extends Controller {
    private Controller nextController;
    private Scene scene;
    private ConnectionContainer connectionContainer;
    private TextField login;
    private PasswordField password;

    public LoginController(Scene scene, ConnectionContainer connectionContainer) {
        this.scene = scene;
        this.connectionContainer = connectionContainer;

        VBox layout = new VBox();
        layout.setSpacing(15);
        layout.setFillWidth(false);
        layout.setAlignment(Pos.CENTER);

        Label loginlabel = new Label("login");
        login = new TextField();

        Label passwordlabel = new Label("password");
        password = new PasswordField();

        Button connect = new Button("login");
        connect.setOnAction(e -> connectToDB());

        layout.getChildren().addAll(loginlabel, login, passwordlabel, password, connect);
        root = layout;

        String propLogin = System.getProperty("login");
        String propPassword = System.getProperty("password");
        if (propLogin != null) {
            login.setText(propLogin);
        }
        if (propPassword != null){
            password.setText(propPassword);
        }
    }

    public void connectToDB(){
        OfficeObjectsRepository repository = new OfficeObjectsRepository();
        Connection connection = repository.createConnection(login.getText(), password.getText());
        if(connection == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Connection failed");
            alert.setHeaderText("Connection failed");
            alert.setContentText("Check your username or password.");
            alert.showAndWait();
            return;
        }
        connectionContainer.setConnection(connection);
        scene.setRoot(nextController.getRoot());
        nextController.display();
    }

    public void setControllers(Controller nextController){
        this.nextController = nextController;
    }
}
