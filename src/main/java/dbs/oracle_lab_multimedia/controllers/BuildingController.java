package dbs.oracle_lab_multimedia.controllers;

import dbs.oracle_lab_multimedia.converters.OfficeComponentConvertor;
import dbs.oracle_lab_multimedia.models.Model;
import dbs.oracle_lab_multimedia.models.OfficeComponentObject;
import dbs.oracle_lab_multimedia.repositories.OfficeObjectsRepository;
import dbs.oracle_lab_multimedia.utils.AlertUtil;
import dbs.oracle_lab_multimedia.utils.ConnectionContainer;
import dbs.oracle_lab_multimedia.utils.SessionContainer;
import dbs.oracle_lab_multimedia.views.DrawingCanvas;
import dbs.oracle_lab_multimedia.views.View;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

/**
 * Controller pro zobrazeni objektu budovy
 */
public class BuildingController extends Controller{
    private DrawingCanvas canvas;
    private Controller photoController;
    private Controller previousController;
    private ConnectionContainer connectionContainer;
    private Scene scene;
    private SessionContainer sessionContainer;
    private OfficeObjectsRepository repository = new OfficeObjectsRepository();
    private ListView<String> objectSelection;

    public BuildingController(Scene scene, ConnectionContainer connectionContainer, SessionContainer sessionContainer){
        this.scene = scene;
        this.sessionContainer = sessionContainer;
        this.connectionContainer = connectionContainer;

        //levy menu
        VBox layout = new VBox();
        layout.setAlignment(Pos.TOP_LEFT);
        layout.setSpacing(10);

        Button backButton = new Button("back");
        backButton.setPrefSize(200, 50);
        backButton.setOnAction(e -> {
            scene.setRoot(previousController.getRoot());
            previousController.display();
        });

        Button photos = new Button("photos");
        photos.setPrefSize(200, 50);
        photos.setOnAction(e -> {
            scene.setRoot(photoController.getRoot());
            photoController.display();
        });

        Button loadDB = new Button("load from DB");
        loadDB.setPrefSize(200, 50);
        loadDB.setOnAction(e -> queryDB());

        Button saveButton = new Button("save");
        saveButton.setPrefSize(200, 50);
        saveButton.setOnAction(e -> save());

        Button remove = new Button("remove object");
        remove.setPrefSize(200, 50);
        remove.setOnAction(e -> delete());

        Button create = new Button("create");
        create.setPrefSize(200, 50);
        create.setOnAction(e -> create());

        Slider slider = new Slider(0.25, 5.0, 1);
        slider.setShowTickLabels(true);
        slider.valueProperty().addListener((observable -> {
            if(canvas.isObjectSelected()){
                canvas.getSelected().resize(slider.getValue());
            }
        }));

        objectSelection = new ListView<String>();
        ObservableList<String> items = FXCollections.observableArrayList();
        items.addAll(OfficeComponentObject.DOOR, OfficeComponentObject.TABLE,
                OfficeComponentObject.WALL);
        objectSelection.setItems(items);

        layout.getChildren().addAll(backButton, photos, loadDB, saveButton, remove, create, slider, objectSelection);

        //Canvas
        canvas = new DrawingCanvas();
        canvas.setMinWidth(3000);
        canvas.setMinHeight(3000);
        canvas.isSelectedProperty().addListener((observableValue, aBoolean, t1) -> {
            if(t1){
                slider.setValue(canvas.getSelected().getScale());
            }
        });
        canvas.setReadonly(false);
        ScrollPane scrollArea = new ScrollPane();
        scrollArea.setContent(canvas);

        //prave menu
        VBox rightMenu = new VBox();
        rightMenu.setAlignment(Pos.TOP_CENTER);
        rightMenu.setSpacing(10);

        Label validateGeomLabel = new Label();
        Button validateGeomButton = new Button("validate geometry");
        validateGeomButton.setPrefSize(200, 50);
        validateGeomButton.setOnAction(e -> {
            if(canvas.getSelected() == null){
                validateGeomLabel.setText("nothing selected");
                return;
            }
            try {
                boolean valid = ((OfficeComponentObject)canvas.getSelected().getModel()).validateGeometry(connectionContainer.getConnection());
                validateGeomLabel.setText(valid ? "geometry valid" : "invalid geometry");
            } catch (SQLException ex) {
                ex.printStackTrace();
                AlertUtil.showDbGeneralError();
            }
        });

        Label totalTableAreaLabel = new Label();
        Button totalTableAreaButton = new Button("total table area");
        totalTableAreaButton.setPrefSize(200, 50);
        totalTableAreaButton.setOnAction(e -> {
            try {
                if(canvas.getSelected() == null){
                    totalTableAreaLabel.setText("nothing selected");
                    return;
                }
                double area = ((OfficeComponentObject)canvas.getSelected().getModel()).totalTableArea(connectionContainer.getConnection());
                totalTableAreaLabel.setText("area: " + area);
            } catch (SQLException ex) {
                ex.printStackTrace();
                AlertUtil.showDbGeneralError();
            }
        });

        Label officePerimeterLabel = new Label();
        Button officePerimeterButton = new Button("office perimeter");
        officePerimeterButton.setPrefSize(200, 50);
        officePerimeterButton.setOnAction(e -> {
            try {
                if(canvas.getSelected() == null){
                    officePerimeterLabel.setText("nothing selected");
                    return;
                }
                double perimeter = ((OfficeComponentObject)canvas.getSelected().getModel()).officePerimeter(connectionContainer.getConnection());
                officePerimeterLabel.setText("perimeter: " + perimeter);
            } catch (SQLException ex) {
                ex.printStackTrace();
                AlertUtil.showDbGeneralError();
            }
        });

        Label distanceLabel = new Label();
        Button distanceButton = new Button("distance between objects");
        distanceButton.setPrefSize(200, 50);
        distanceButton.setOnAction(e -> {
            try {
                if(canvas.getSelected() == null){
                    distanceLabel.setText("nothing selected");
                    return;
                }
                if(canvas.getPreviouslySelected() == null){
                    distanceLabel.setText("select second object");
                    return;
                }
                double distance = ((OfficeComponentObject)canvas.getSelected().getModel()).distance(connectionContainer.getConnection(),
                        (OfficeComponentObject) canvas.getPreviouslySelected().getModel());
                distanceLabel.setText("distance: " + distance);
            } catch (SQLException ex) {
                ex.printStackTrace();
                AlertUtil.showDbGeneralError();
            }
        });

        Label boundedLabel = new Label();
        Button boundedButton = new Button("find object outside office");
        boundedButton.setPrefSize(200, 50);
        boundedButton.setOnAction(e -> {
            try {
                if (canvas.getSelected() == null) {
                    boundedLabel.setText("nothing selected");
                    return;
                }
                for (Node v : canvas.getChildren()) {
                    View view = (View) v;
                    OfficeComponentObject officeComponentObject = (OfficeComponentObject) view.getModel();
                    if (officeComponentObject.getType().equals(OfficeComponentObject.OFFICE))
                        continue;
                    if (!officeComponentObject.isBoundedByOfficeSpace(connectionContainer.getConnection())) {
                        view.select();
                    } else {
                        view.deselect();
                    }
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
                AlertUtil.showDbGeneralError();
            }
        });

        rightMenu.getChildren().addAll(validateGeomLabel, validateGeomButton, totalTableAreaLabel, totalTableAreaButton,
                officePerimeterLabel, officePerimeterButton, distanceLabel, distanceButton, boundedLabel, boundedButton);

        //horni menu
        MenuBar menuBar = new MenuBar();

        Menu menu1 = new Menu("DB edit");
        MenuItem menuItem1 = new MenuItem("Create tables");
        menuItem1.setOnAction(e -> createTables());
        MenuItem menuItem2 = new MenuItem("Delete tables");
        menuItem2.setOnAction(e -> dropTables());
        MenuItem menuItem3 = new MenuItem("Create index");
        menuItem3.setOnAction(e -> createIndex());
        menu1.getItems().addAll(menuItem1, menuItem2, menuItem3);

        menuBar.getMenus().add(menu1);

        //hlavni layout
        BorderPane mainLayout = new BorderPane();
        mainLayout.setLeft(layout);
        mainLayout.setCenter(scrollArea);
        mainLayout.setTop(menuBar);
        mainLayout.setRight(rightMenu);
        root = mainLayout;
    }

    @Override
    public void display(){
        queryDB();
    }

    public void setControllers(Controller photoController, Controller previousController){
        this.photoController = photoController;
        this.previousController = previousController;
    }

    private void save(){
        ObservableList<Node> views = canvas.getChildren();
        for(Node n : views){
            Model model = ((View) n).getModel();
            OfficeComponentConvertor.updateModel((View) n);
            try {
                ((View) n).getModel().update(connectionContainer.getConnection());
            } catch (SQLException e) {
                e.printStackTrace();
                AlertUtil.showDbGeneralError();
                return;
            }
        }
    }

    private void create() {
        OfficeComponentObject object;
        String objectType = objectSelection.getSelectionModel().getSelectedItem();
        if(objectType == null)
            return;
        try {
            object = OfficeComponentObject.createSampleAndStore2DB(connectionContainer.getConnection(), sessionContainer.getOfficeObject(), objectType);
        } catch (SQLException e) {
            e.printStackTrace();
            AlertUtil.showDbGeneralError();
            return;
        }
        View view = OfficeComponentConvertor.toView(object);
        canvas.add(view);
        canvas.select(view);
    }

    private void queryDB() {
        List<OfficeComponentObject> officeObjects = repository.getObjects(connectionContainer.getConnection(), sessionContainer.getOfficeObject());
        canvas.clear();
        for(OfficeComponentObject o : officeObjects){
            View view = OfficeComponentConvertor.toView(o);
            if(o.getType().equals(OfficeComponentObject.OFFICE))
                canvas.addOffice(view);
            else
                canvas.add(view);
        }
    }

    private void delete(){
        if(canvas.getSelected() == null)
            return;
        View selected = canvas.getSelected();
        try {
            selected.getModel().delete(connectionContainer.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
            AlertUtil.showDbGeneralError();
        }
        canvas.remove(selected);
    }

    private void createTables(){
        try {
            repository.scriptRunner(connectionContainer.getConnection(), "create_tables.sql");
        } catch (SQLException | FileNotFoundException ex) {
            ex.printStackTrace();
            AlertUtil.showGeneralError("Failed tp create tables.");
            return;
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Success");
        alert.setHeaderText("Operation successful");
        alert.setContentText("DB tables created");
        alert.showAndWait();

        scene.setRoot(previousController.getRoot());
        previousController.display();
    }

    private void dropTables(){
        try {
            repository.scriptRunner(connectionContainer.getConnection(), "drop_tables.sql");
        } catch (SQLException | FileNotFoundException ex) {
            ex.printStackTrace();
            AlertUtil.showGeneralError("Drop tables failed.");
            return;
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Success");
        alert.setHeaderText("Operation successful");
        alert.setContentText("DB tables created");
        alert.showAndWait();

        scene.setRoot(previousController.getRoot());
        previousController.display();
    }

    private void createIndex() {
        OfficeObjectsRepository repository = new OfficeObjectsRepository();
        try {
            repository.scriptRunner(connectionContainer.getConnection(), "create_index.sql");
        } catch (SQLException | FileNotFoundException ex) {
            ex.printStackTrace();
            AlertUtil.showGeneralError("Create index failed.");
            return;
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Success");
        alert.setHeaderText("Operation successful");
        alert.setContentText("Index created");
        alert.showAndWait();
    }
}
