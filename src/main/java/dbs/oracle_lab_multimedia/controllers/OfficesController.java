package dbs.oracle_lab_multimedia.controllers;

import dbs.oracle_lab_multimedia.models.OfficeObject;
import dbs.oracle_lab_multimedia.repositories.OfficeObjectsRepository;
import dbs.oracle_lab_multimedia.utils.AlertUtil;
import dbs.oracle_lab_multimedia.utils.ConnectionContainer;
import dbs.oracle_lab_multimedia.utils.SessionContainer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

public class OfficesController extends Controller{
    private Scene scene;
    private ConnectionContainer connectionContainer;
    private SessionContainer sessionContainer;
    private TextField newOfficeTextField;
    private ListView<String> listView;
    private List<OfficeObject> officeObjects;
    private Controller nextController;
    private Controller newOfficeController;

    public OfficesController(Scene scene, ConnectionContainer connectionContainer, SessionContainer sessionContainer) {
        this.scene = scene;
        this.connectionContainer = connectionContainer;
        this.sessionContainer = sessionContainer;

        //horni menu
        MenuBar menuBar = new MenuBar();

        Menu menu1 = new Menu("DB edit");
        MenuItem menuItem1 = new MenuItem("Create tables");
        menuItem1.setOnAction(e -> createTables());
        MenuItem menuItem2 = new MenuItem("Delete tables");
        menuItem2.setOnAction(e -> dropTables());
        MenuItem menuItem3 = new MenuItem("Create index");
        menuItem3.setOnAction(e -> createIndex());
        menu1.getItems().addAll(menuItem1, menuItem2, menuItem3);

        menuBar.getMenus().add(menu1);

        //Office view
        listView = new ListView<String>();
        listView.setOnMouseClicked(e -> {
            if(e.getClickCount() == 2 && listView.getSelectionModel().getSelectedItem() != null){
                OfficeObject officeObject = officeObjects.get(listView.getSelectionModel().getSelectedIndex());
                sessionContainer.setOfficeObject(officeObject);
                scene.setRoot(nextController.getRoot());
                nextController.display();
            }
        });

        //dolni menu
        Label newOfficeLabel = new Label("Office company name:");
        newOfficeTextField = new TextField("name");
        Button refreshButton = new Button("refresh list");
        refreshButton.setOnAction(e -> loadOffices());
        Button deleteButton = new Button("delete selected office");
        deleteButton.setOnAction(e -> deleteOffice());
        Button createNewOfficeButton = new Button("create office");
        createNewOfficeButton.setOnAction(e -> createNewOffice());

        HBox newOfficeLayout = new HBox();
        newOfficeLayout.getChildren().addAll(refreshButton, deleteButton, newOfficeLabel, newOfficeTextField, createNewOfficeButton);

        //main layout
        VBox mainLayout = new VBox();
        mainLayout.getChildren().addAll(menuBar, listView, newOfficeLayout);
        root = mainLayout;
    }

    @Override
    public void display(){
        loadOffices();
    }

    public void setControllers(Controller nextController, Controller newOfficeController){
        this.nextController = nextController;
        this.newOfficeController = newOfficeController;
    }

    public void loadOffices(){
        OfficeObjectsRepository repository = new OfficeObjectsRepository();
        officeObjects =  repository.getOffices(connectionContainer.getConnection());
        ObservableList<String> items = FXCollections.observableArrayList();
        for(OfficeObject o: officeObjects){
            items.add(o.getCompany());
        }
        listView.setItems(items);
    }

    private void createNewOffice(){
        OfficeObject office = new OfficeObject();
        office.setCompany(newOfficeTextField.getText());
        try {
            office.update(connectionContainer.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
            AlertUtil.showDbGeneralError();
            return;
        }
        sessionContainer.setOfficeObject(office);
        scene.setRoot(newOfficeController.getRoot());
        newOfficeController.display();
    }

    private void deleteOffice(){
        int selected = listView.getSelectionModel().getSelectedIndex();
        try {
            officeObjects.get(selected).delete(connectionContainer.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
            AlertUtil.showDbGeneralError();
        }
        loadOffices();
    }

    private void createTables(){
        OfficeObjectsRepository repository = new OfficeObjectsRepository();
        try {
            repository.scriptRunner(connectionContainer.getConnection(), "create_tables.sql");
        } catch (SQLException | FileNotFoundException ex) {
            ex.printStackTrace();
            AlertUtil.showGeneralError("Failed tp create tables.");
            return;
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Success");
        alert.setHeaderText("Operation successful");
        alert.setContentText("DB tables created");
        alert.showAndWait();

        loadOffices();
    }

    private void dropTables(){
        OfficeObjectsRepository repository = new OfficeObjectsRepository();
        try {
            repository.scriptRunner(connectionContainer.getConnection(), "drop_tables.sql");
        } catch (SQLException | FileNotFoundException ex) {
            ex.printStackTrace();
            AlertUtil.showGeneralError("Drop tables failed.");
            return;
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Success");
        alert.setHeaderText("Operation successful");
        alert.setContentText("DB tables created");
        alert.showAndWait();

        loadOffices();
    }

    private void createIndex() {
        OfficeObjectsRepository repository = new OfficeObjectsRepository();
        try {
            repository.scriptRunner(connectionContainer.getConnection(), "create_index.sql");
        } catch (SQLException | FileNotFoundException ex) {
            ex.printStackTrace();
            AlertUtil.showGeneralError("Create index failed.");
            return;
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Success");
        alert.setHeaderText("Operation successful");
        alert.setContentText("Index created");
        alert.showAndWait();
    }
}
