package dbs.oracle_lab_multimedia.controllers;

import dbs.oracle_lab_multimedia.converters.OfficeComponentConvertor;
import dbs.oracle_lab_multimedia.models.OfficeComponentObject;
import dbs.oracle_lab_multimedia.utils.AlertUtil;
import dbs.oracle_lab_multimedia.utils.ConnectionContainer;
import dbs.oracle_lab_multimedia.utils.SessionContainer;
import dbs.oracle_lab_multimedia.views.PolygonView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

import java.sql.SQLException;

public class OfficeCreationController extends Controller{
    private ConnectionContainer connectionContainer;
    private Controller nextController;
    private Controller previousController;
    private Scene scene;
    private SessionContainer sessionContainer;
    private Pane canvas;
    private Polygon polygon;

    public OfficeCreationController(Scene scene, ConnectionContainer connectionContainer, SessionContainer sessionContainer) {
        this.scene = scene;
        this.sessionContainer = sessionContainer;
        this.connectionContainer = connectionContainer;

        //center
        canvas = new Pane();
        canvas.setOnMouseClicked(this::makePoint);

        //leve menu
        VBox leftmenu = new VBox();
        leftmenu.setAlignment(Pos.TOP_LEFT);
        leftmenu.setSpacing(10);
        leftmenu.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, Insets.EMPTY)));

        Button backButton = new Button("back");
        backButton.setPrefWidth(200);
        backButton.setOnAction(e -> {
            scene.setRoot(previousController.getRoot());
            previousController.display();
        });

        Button doneButton = new Button("done");
        doneButton.setPrefWidth(200);
        doneButton.setOnAction(e -> done());

        Button undoButton = new Button("undo");
        undoButton.setPrefWidth(200);
        undoButton.setOnAction(e -> {
            if(polygon.getPoints().size() == 0)
                return;
            polygon.getPoints().remove(polygon.getPoints().size() - 1); //y
            polygon.getPoints().remove(polygon.getPoints().size() - 1); //x
        });

        leftmenu.getChildren().addAll(backButton, doneButton, undoButton);

        //hlavni layout
        BorderPane mainLayout = new BorderPane();
        mainLayout.setLeft(leftmenu);
        mainLayout.setCenter(canvas);
        root = mainLayout;
    }

    @Override
    public void display(){
        polygon = new Polygon();
        polygon.setFill(Color.TRANSPARENT);
        polygon.setStroke(Color.BLACK);
        polygon.setStrokeWidth(5);
        canvas.getChildren().clear();
        canvas.getChildren().add(polygon);
    }

    private void makePoint(MouseEvent e) {
        polygon.getPoints().add(e.getX());
        polygon.getPoints().add(e.getY());
    }

    public void setControllers(Controller nextController, Controller previousController){
        this.nextController = nextController;
        this.previousController = previousController;
    }

    private void done() {
        OfficeComponentObject object = new OfficeComponentObject();
        object.setType(OfficeComponentObject.OFFICE);
        object.setOfficeId(sessionContainer.getOfficeObject().getOfficeId());
        PolygonView polygonView = new PolygonView(polygon, object);
        OfficeComponentConvertor.updateModel(polygonView);
        try {
            object.update(connectionContainer.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
            AlertUtil.showDbGeneralError();
            scene.setRoot(previousController.getRoot());
            previousController.display();
            return;
        }
        scene.setRoot(nextController.getRoot());
        nextController.display();
    }
}
