package dbs.oracle_lab_multimedia.controllers;
import dbs.oracle_lab_multimedia.utils.ConnectionContainer;
import dbs.oracle_lab_multimedia.utils.SessionContainer;
import dbs.oracle_lab_multimedia.models.PersonObject;
import dbs.oracle_lab_multimedia.utils.AlertUtil;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

import java.sql.SQLException;

public class PersonDetailController extends Controller {
    private PersonObject person;
    private Image picture;
    ImageView iv = new ImageView();
    private final SessionContainer sessionContainer;
    private PhotosController photosController;
    private Text nameText = new Text();
    private Text dateText;
    private PersonEditController personEditController;
    public PersonDetailController(final Scene scene, final ConnectionContainer connectionContainer,
        final SessionContainer sessionContainer) {

        this.sessionContainer = sessionContainer;
        VBox layout = new VBox();
        layout.setSpacing(10);

        nameText.setStyle("-fx-font-size: 24;");
        layout.getChildren().add(nameText);

        layout.getChildren().add(iv);
        layout.setAlignment(Pos.CENTER);

        dateText = new Text();
        layout.getChildren().add(dateText);

        BorderPane mainLayout = new BorderPane();
        mainLayout.setCenter(layout);

        Button backButton = new Button("back");
        backButton.setPrefSize(200, 60);
        backButton.setOnAction(e -> {
            scene.setRoot(photosController.getRoot());
            photosController.display();
        });

        Button editButton = new Button("edit");
        editButton.setPrefSize(200, 60);

        editButton.setOnAction(e -> {
            scene.setRoot(personEditController.getRoot());
            personEditController.display();
        });

        HBox buttons = new HBox();
        buttons.setSpacing(10);
        buttons.getChildren().addAll(backButton, editButton);
        mainLayout.setBottom(buttons);

        root = mainLayout;
    }

    private void loadImageFromDB () throws SQLException {
        this.picture = new Image(person.getPhoto().getBlobContent().getBinaryStream());
        iv.setImage(this.picture);
    }

    private void loadPerson() {
        this.person = sessionContainer.getPersonObject();

        nameText.setText(person.getName());
        dateText.setText(person.getDate().toString());
    }
    @Override
    public void display(){
        loadPerson();
        try {
            loadImageFromDB();
        }
        catch (SQLException e) {
            e.printStackTrace();
            AlertUtil.showDbGeneralError();
        }
    }

    public void setControllers(final PhotosController photosController, final PersonEditController personEditController) {
        this.photosController = photosController;
        this.personEditController = personEditController;
    }
}