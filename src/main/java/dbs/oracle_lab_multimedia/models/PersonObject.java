package dbs.oracle_lab_multimedia.models;

import oracle.jdbc.OraclePreparedStatement;
import oracle.ord.im.OrdImage;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.text.ParseException;
import oracle.jdbc.*;
import oracle.ord.im.*;

/**
 * Backend model, reprezentuje všechny objekty v budově.
 *
 * Pravděpodobně budete chtít zdědit.
 */
public class PersonObject extends Model {
    private String name = "";
    public Date birth;
    private int desk_id = -1;
    private int office_id = -1;
    private OrdImage photo;

    // getter and setters
    // name
    public void setName(String s) {
        name = s;
        setModified();
    }
    public String getName() {
        return name;
    }

    // birth
    // returns 0 if date string was formatted correctly, 1 otherwise
    // Expects yyyy-MM-dd format
    public int setDate(String date){
        try {
            birth = Date.valueOf(date);
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
        setModified();
        return 0;
    }
    public void setDate(Date date){ birth=date;}
    public Date getDate() {return birth;}

    // assign desk
    public void assignTable(OfficeComponentObject desk)  throws RuntimeException {
        if (desk.getType() != "desk") {
            throw new RuntimeException(
                    "Trying to assign %s to person %s".format(desk.getType(), name));
        }
        if (desk.getId() == -1) {
            throw new RuntimeException(
                    "Table doesn't have generated id. Run office.update(connection) to generate id.");
        }
        setModified();
        desk_id = desk.getId();
    }
    public void unassignTable() {desk_id = -1;setModified();}
    public void setTable(int id) {desk_id = id;setModified();}
    public int  getTable() {return desk_id;}

    public void assignOffice(OfficeObject office)  throws RuntimeException {
        if (office.getId() == -1) {
            throw new RuntimeException(
                    "Office doesn't have generated id. Run office.update(connection) to generate id.");
        }
        setModified();
        office_id = office.getId();
    }
    public void setOfficeId(int id){office_id = id;setModified();}
    public int  getOfficeId(){return office_id;}

    // Photo
    public void setPhoto(OrdImage photo){this.photo = photo;setModified();}

    public OrdImage getPhoto() {return this.photo;}
    /*
     if persona_id is in db (not -1) then image should be present locally
     if not in table insert all data and then call updateOrdImage()
     if in table only call updateOrdImage
     there is the thing with selectOrdImageForUpdate why?
     */
    private void updateTable(Connection conn)
            throws RuntimeException, SQLException {
        if (name == ""){
            throw new RuntimeException("A person has not assigned a name");
        }
        if (birth == null){
            throw new RuntimeException("A person has not assigned a date of birth");
        }
        if (office_id == -1) {
            throw new RuntimeException("A person %s has not assigned office".format(name));
        }
        if (id == -1) {
            // inserting a new record
            String sql = "INSERT INTO person(name, birth, desk_id, office_id, photo)" +
                        " VALUES (?,?,?,?, ordsys.ordimage.init())";
            PreparedStatement stmt;
            String returnCols[] = { "person_id" };
            stmt = conn.prepareStatement(sql, returnCols);
            stmt.setString(1, name);
            stmt.setDate(2, birth);
            if (desk_id != -1) {
                stmt.setNull(3, Types.INTEGER);
            } else {
                stmt.setNull(3, desk_id);
            }
            stmt.setInt(4, office_id);

            int updated = stmt.executeUpdate();
            // fetching generated keys
            if (updated == 0) {
                throw new SQLException("Creating person failed");
            }
            try (ResultSet keys = stmt.getGeneratedKeys()) {
                if (keys.next()) {
                    this.setId(keys.getInt(1));
                } else {
                    throw new SQLException("Unable to fetch keys.");
                }
            }
        } else {
            String sql_update = "UPDATE person SET name =  ?, " +
                    "birth = ?, desk_id = ?, office_id = ? WHERE person_id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql_update);
            stmt.setString(1, name);
            stmt.setDate(2, birth);
            if (desk_id != -1) {
                stmt.setNull(3, Types.INTEGER);
            } else {
                stmt.setNull(3, desk_id);
            }
            stmt.setInt(4, office_id);
            stmt.setInt(5, id);
            stmt.executeUpdate();

        }

    }

    private OrdImage selectPhotoForUpdate(Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "SELECT photo FROM person WHERE person_id = " + id + " FOR UPDATE";
        OracleResultSet rset = (OracleResultSet) stmt.executeQuery(sql);
        if (rset.next())
            return (OrdImage) rset.getORAData("photo", OrdImage.getORADataFactory());
        else
            throw new SQLException("No record found.");
    }
    private OrdImage fetchPhotoForUpdate(Connection conn) throws SQLException {
        Statement stmt = conn.createStatement();
        String sql = "SELECT photo FROM person WHERE person_id = " + id;
        OracleResultSet rset = (OracleResultSet) stmt.executeQuery(sql);
        if (rset.next())
            return (OrdImage) rset.getORAData("photo", OrdImage.getORADataFactory());
        else
            throw new SQLException("No record found.");
    }

    /**
     * Updates object in DB with photo loaded from file.
     * <p>
     * If object is being added to DB, id is generated in DB and set to the
     * object. If operation with DB fails, then throw exception.
     * <p>
     * PersonObject must have assigned office before updating.
     * When office is not assigned a RuntimeException is thrown.
     *
     * @param  connection  connection to database
     * @param  filename    path to photo
     * @see          Connection
     * @see          OfficeComponentObject
     */
    public void update(Connection connection, String filename)
            throws SQLException, IOException {
        final boolean previousAutoCommit = connection.getAutoCommit();
        connection.setAutoCommit(false);
        String sql = "UPDATE person SET photo = ? WHERE person_id = ?";
        try (PreparedStatement stmt1 = connection.prepareStatement(sql)) {
            updateTable(connection);
            photo = selectPhotoForUpdate(connection);
            System.out.println("File with photo: "+ filename);
            photo.loadDataFromFile(filename);
            photo.setProperties();
            OraclePreparedStatement ostmt = (OraclePreparedStatement) stmt1;
            ostmt.setORAData(1, photo);
            ostmt.setInt(2, id);
            ostmt.executeUpdate();
            // updating metadata and still image
            sql = "UPDATE person p SET p.photo_si = "+
                    "SI_StillImage(p.photo.getContent()) WHERE p.person_id = ?";
            final PreparedStatement stmt2 = connection.prepareStatement(sql);
            stmt2.setInt(1, id);
            stmt2.executeUpdate();
            sql = "UPDATE person SET photo_ac = SI_AverageColor(photo_si), "+
                    "photo_ch = SI_ColorHistogram(photo_si), "+
                    "photo_pc = SI_PositionalColor(photo_si), "+
                    "photo_tx = SI_Texture(photo_si) WHERE person_id = ?";
            final PreparedStatement stmt3 = connection.prepareStatement(sql);
            stmt3.setInt(1, id);
            stmt3.executeUpdate();
            this.photo = fetchPhotoForUpdate(connection);
        } finally {
            connection.setAutoCommit(previousAutoCommit);
        }
    }

    @Override
    public void update(Connection connection) throws SQLException {
        updateTable(connection);
    }

    @Override
    public void delete(Connection conn) throws SQLException {
        if (id == -1){
            // object was not added to db yet
            return;
        }
        String sql = "DELETE FROM person WHERE person_id = ?";
        PreparedStatement stmt;
        stmt = conn.prepareStatement(sql);
        stmt.setInt(1, id);
        stmt.executeUpdate();
        return;
    }

    public PersonObject getPersonByPhoto(Connection conn) throws SQLException {
        PersonObject person = new PersonObject();
        String sql = "SELECT dst.person_id, SI_ScoreByFtrList(new SI_FeatureList(src.photo_ac,0.3,src.photo_ch,0.3,src.photo_pc,0.1,src.photo_tx,0.3),dst.photo_si) AS similarity FROM person src, person dst WHERE (src.person_id = ?) AND (src.person_id <> dst.person_id) ORDER BY similarity ASC";

        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();

        int similar;
        if (rs.next())
            similar = rs.getInt("person_id");
        else
            return null;

        sql = "Select office_id, name, birth, desk_id, photo "+
                "from person where person_id = ?";
        stmt = conn.prepareStatement(sql);
        stmt.setInt(1, similar);
        rs = stmt.executeQuery();

        if (rs.next()){
            person.setId(similar);
            person.setOfficeId(rs.getInt("office_id"));
            person.setName(rs.getString("name"));
            person.setDate(rs.getDate("birth"));
            person.setTable(rs.getInt("desk_id"));
            if (rs.wasNull()){
                person.setTable(-1);
            }
            final OracleResultSet ors = (OracleResultSet) rs;
            person.setPhoto((OrdImage) ors.getORAData("photo", OrdImage.getORADataFactory()));

        } else {
            throw new RuntimeException("Initialisation of new person failed.");
        }
        return person;
    }

    public void convert2Greyscale(Connection conn) throws SQLException {
        System.out.println("Photo processed");
        OrdImage photo = selectPhotoForUpdate(conn);
        String sql = "UPDATE person SET photo = ? WHERE person_id = ?";
        PreparedStatement stmt1 = conn.prepareStatement(sql);
        OraclePreparedStatement ostmt = (OraclePreparedStatement) stmt1;
        photo.process("contentFormat=8bitgreyscale");
        ostmt.setORAData(1, photo);
        ostmt.setInt(2, id);
       ostmt.executeUpdate();
        // updating metadata and still image
        sql = "UPDATE person p SET p.photo_si = "+
                "SI_StillImage(p.photo.getContent()) WHERE p.person_id = ?";
        final PreparedStatement stmt2 = conn.prepareStatement(sql);
        stmt2.setInt(1, id);
        stmt2.executeUpdate();
        sql = "UPDATE person SET photo_ac = SI_AverageColor(photo_si), "+
                "photo_ch = SI_ColorHistogram(photo_si), "+
                "photo_pc = SI_PositionalColor(photo_si), "+
                "photo_tx = SI_Texture(photo_si) WHERE person_id = ?";
        final PreparedStatement stmt3 = conn.prepareStatement(sql);
        stmt3.setInt(1, id);
        stmt3.executeUpdate();
        this.photo = fetchPhotoForUpdate(conn);
    }

    /**
     * Creates an PersonObject object and assigns a photo to it. This method is for testing and
     * building FE. Created object is automatically stored in DB with photo.
     *
     * @param conn   database connection
     * @param office office object to provide office_id
     * @param number index of person (1 to 5)
     * @return       PersonObject object
     * @see          OfficeObject
     * @see          Connection
     */
    public static PersonObject createSampleAndStore2DB(Connection conn, OfficeObject office, int number)
            throws RuntimeException, IOException, SQLException
    {
        PersonObject person = new PersonObject();
        person.assignOffice(office);
        person.setDate("1994-12-14");
        switch (number){
            case 1:
                person.setName("Emma");
                break;
            case 2:
                person.setName("Karol");
                break;
            case 3:
                person.setName("Elena");
                break;
            case 4:
                person.setName("Norbert");
                break;
            case 5:
                person.setName("Zuzana");
                break;
            default:
                throw new RuntimeException("Invalid person number.");
        }
        //String file = new Strings
        person.update(conn, "./data/photos/00000"+number+".jpg");
        return person;
    }
}
