package dbs.oracle_lab_multimedia.models;

import java.sql.*;

/**
 * Backend model, reprezentuje všechny objekty v budově.
 *
 * Pravděpodobně budete chtít zdědit.
 */
public class OfficeObject extends Model {
    // this will be useful for geometric shapes where we can postpone fetching data
    // private boolean fetched = False;
    private String  company;

    // getters and setters
    // company
    public void setCompany(String co){company = co; setModified();}
    public String getCompany(){return company;}
    // id
    // THESE METHODS ARE ONLY FOR OfficeObjectsRepository!!
    public void setOfficeId(int id){this.id = id;}
    public int getOfficeId(){return id;}

    @Override
    public void update(Connection connection) throws SQLException
    {
        // no
        if (!modified)
            return;
        String sql_insert = "INSERT INTO office (company) VALUES (?)";
        String sql_update = "UPDATE office SET company =  ? WHERE office_id = ?";
        try {
            // if id was not set office has to be inserted
            PreparedStatement stmt;
            if (id == -1) {
                int updated = -1;
                String returnCols[] = { "office_id" };
                stmt = connection.prepareStatement(sql_insert, returnCols);
                stmt.setString(1, company);
                updated = stmt.executeUpdate();
                // fetching generated keys
                if (updated == 0){
                    throw new SQLException("Creating office failed.");
                }
                try (ResultSet keys = stmt.getGeneratedKeys()){
                    if (keys.next()) {
                        this.setOfficeId(keys.getInt(1));
                    } else {
                        throw new SQLException("Unable to fetch keys.");
                    }
                }
            } else {
                // object was fetched and modified
                stmt = connection.prepareStatement(sql_update);
                stmt.setString(1, company);
                stmt.setInt(2, id);
               stmt.executeUpdate();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("DB operation failed.");
        }
    }

    @Override
    public void delete(Connection conn) throws SQLException {
        if (id == -1){
            // object was not added to db yet
            return;
        }
        String sql = "DELETE FROM office WHERE office_id = ?";
        PreparedStatement stmt;
        stmt = conn.prepareStatement(sql);
        stmt.setInt(1, id);
        stmt.executeUpdate();
        return;
    }

    /**
     * Creates an office object. This method is for testing and building FE. Created object is
     * automatically stored in DB.
     * @param conn connection to DB
     * @return new office created and stored in database
     * @throws SQLException
     */
    public static OfficeObject createSampleAndStore2DB(Connection conn) throws SQLException {
        OfficeObject office = new OfficeObject();
        office.setCompany("FIT");
        office.update(conn);
        return office;
    }
/* end of OfficeObject */
}
