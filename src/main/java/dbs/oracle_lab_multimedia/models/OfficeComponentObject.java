package dbs.oracle_lab_multimedia.models;

import oracle.spatial.geometry.JGeometry;
import oracle.sql.STRUCT;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Backend model, reprezentuje všechny objekty v budově.
 *
 * This class represents office_objects table
 * Pravděpodobně budete chtít zdědit.
 */
public class OfficeComponentObject extends Model {
    // this will be useful for geometric shapes where we can postpone fetching data
    // private boolean fetched = False;
    private boolean modified = false;
    private String  type = "";
    private int     office_id = -1;
    private JGeometry geometry = null;

    // TYPES
    public final static String TABLE = "table";
    public final static String OFFICE = "office";
    public final static String WALL = "wall";
    public final static String DOOR = "door";

    // getters and setters
    // modified
    public void setModified(){modified=true;}
    public void unsetModified(){modified=false;}
    /**
     * Sets object type. This method sets modified flag.
     * <p>
     * Object types to be used:\n
     *      OfficeComponentObject.TABLE \n
     *      OfficeComponentObject.OFFICE \n
     *      OfficeComponentObject.WALL \n
     *      OfficeComponentObject.DOOR \n
     *
     * @param  t  object type
     * @see          OfficeComponentObject
     */
    public void setType(String t){type = t; setModified();}
    /**
     * Returns object type of an object.
     * <p>
     * Object types to be used:\n
     *      OfficeComponentObject.TABLE \n
     *      OfficeComponentObject.OFFICE \n
     *      OfficeComponentObject.WALL \n
     *      OfficeComponentObject.DOOR \n
     *
     * @return       String  object type
     * @see          OfficeComponentObject
     */
    public String getType(){return type;}
    /**
     * Assign office id to an object. Each object from OfficeComponentObject
     * or PersonObject has to be assigned to an office.
     * <p>
     * This method sets modified flag.
     *
     * @param office an office that we want the object to be assigned to.
     *               Office must have generated ID. If new office was created,
     *               run `office.update(conn)` first.
     * @see          OfficeObject
     */
    public void assignOffice(OfficeObject office) throws RuntimeException {
        if (office.getId() == -1) {
            throw new RuntimeException(
                    "Office doesn't have generated id. Run office.update(connection) to generate id.");
        }
        setModified();
        office_id = office.getId();
    }
    /**
     * Assign office id to an object. Each object from OfficeComponentObject
     * or PersonObject has to be assigned to an office.
     * <p>
     * This method sets modified flag.
     * <p>
     * This method should be used only from OfficeObjectsRepository and Model classes.
     *
     * @param id    an office id.
     * @see          OfficeObject
     */
    public void setOfficeId(int id){office_id = id;setModified();}
    /**
     * Returns assigned office id to an object. Each object from OfficeComponentObject
     * or PersonObject has to be assigned to an office.
     * <p>
     * This method should be used only from OfficeObjectsRepository and Model classes.
     *
     * @return       an office id.
     * @see          OfficeObject
     */
    public int getOfficeId(){return office_id;}
    /**
     * Sets geometry.
     *
     * @param geo    JGeometry object.
     * @see          JGeometry
     */
    public void setGeometry(JGeometry geo){geometry = geo;}
    public void setGeoPoint(double[] coordinates){ geometry = JGeometry.createPoint(coordinates,2,0);}
    public void setGeoPolygon(double[] coordinates){ geometry = JGeometry.createLinearPolygon(coordinates, 2, 0);}
    public void setGeoLine(double[] coordinates){ geometry = JGeometry.createLinearLineString(coordinates, 2, 0);}
    public void setGeoCircle(double[] coordinates, double radius)
    { geometry = JGeometry.createCircle(coordinates[0], coordinates[1], radius, 0);}
    /**
     * Returns geometry.
     *
     * @return       JGeometry from DB.
     * @see          JGeometry
     */
    public JGeometry getGeometry(){return geometry;}
    // TODO some interface for manipulating geometry

    @Override
    public void update(Connection connection) throws RuntimeException, SQLException
    {
        if (!modified)
            return;
        if (office_id == -1){
            throw new RuntimeException("Object was not assigned to an office.");
        }
        String sql_insert = "INSERT INTO office_object (type, office_id, geometry) VALUES (?, ?, ?)";
        String sql_update = "UPDATE office_object SET type =  ?, office_id = ?, geometry = ? WHERE object_id = ?";
        try {
            PreparedStatement stmt;
            if (id == -1) {
                // object id is not set so new object is created
                String returnCols[] = { "object_id" };
                stmt = connection.prepareStatement(sql_insert, returnCols);
                stmt.setString(1, type);
                stmt.setInt(2, office_id);
                // couldn't find any other way although this is "depreciated"
                STRUCT obj;
                obj = JGeometry.store(geometry, connection);
                stmt.setObject(3, obj);
                int updated = stmt.executeUpdate();
                // fetching generated keys
                if (updated == 0){
                    throw new SQLException("Creating office failed.");
                }
                try (ResultSet keys = stmt.getGeneratedKeys()){
                    if (keys.next()) {
                        this.setId(keys.getInt(1));
                    } else {
                        throw new SQLException("Unable to fetch keys.");
                    }
                }
            } else {
                // updating existing object
                stmt = connection.prepareStatement(sql_update);
                stmt.setString(1, type);
                stmt.setInt(2, office_id);
                // couldn't find any other way although this is "depreciated"
                STRUCT obj;
                obj = JGeometry.store(geometry, connection);
                stmt.setObject(3, obj);

                stmt.setInt(4, id); // where clause
                stmt.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("DB operation failed.");
        }
    }

    @Override
    public void delete(Connection conn) throws SQLException {
        if (id == -1){
            // object was not added to db yet
            return;
        }
        String sql = "DELETE FROM office_object WHERE object_id = ?";
        PreparedStatement stmt;
        stmt = conn.prepareStatement(sql);
        stmt.setInt(1, id);
        stmt.executeUpdate();
        return;
    }

    /**
     * Creates an object with geometry to be displayed with one sample of geometry. This method is for testing and
     * building FE. It can create object of every type depending on type to populate office. Created object is
     * automatically stored in DB.
     * <p>
     * Types to be passed to this function:\n
     *      OfficeComponentObject.TABLE \n
     *      OfficeComponentObject.OFFICE \n
     *      OfficeComponentObject.WALL \n
     *      OfficeComponentObject.DOOR \n
     *
     * @param conn   database connection
     * @param office office object to provide office_id
     * @param type   one of object types
     * @return       OfficeComponentObject object of requested type
     * @see          OfficeObject
     * @see          Connection
     */
    public static OfficeComponentObject createSampleAndStore2DB(Connection conn, OfficeObject office, String type)
            throws SQLException, RuntimeException
    {
        OfficeComponentObject obj = new OfficeComponentObject();
        obj.setType(type);
        obj.assignOffice(office);
        switch (type){
            case OfficeComponentObject.OFFICE:
                double coords1[] = {0,0, 50,0, 50,30, 100,30, 100,60, 0,60, 0,0};
                obj.setGeometry(JGeometry.createLinearPolygon(coords1, 2, 0));
                break;
            case OfficeComponentObject.TABLE:
                obj.setGeometry(JGeometry.createLinearPolygon(new double[]{
                        30, 30,
                        150, 30,
                        150, 90,
                        30, 90
                }, 2, 0));
                break;
            case OfficeComponentObject.DOOR:
                obj.setGeometry(JGeometry.createLinearPolygon(new double[]{
                        30, 30,
                        40, 30,
                        40, 60,
                        30, 60
                }, 2, 0));
                break;
            case OfficeComponentObject.WALL:
                double coords2[] = {0,30, 200,30};
                obj.setGeometry(JGeometry.createLinearLineString(coords2, 2, 0));
                break;
            default:
                throw new RuntimeException("Unknown object type.");
        }
        obj.update(conn);
        return obj;
    }

    // SPATIAL OPERATOR 1 (in where clause)
    public boolean isBoundedByOfficeSpace(Connection conn) throws SQLException {
        if (id == -1)
            throw new RuntimeException("Object was not stored to DB");

        String sql = "SELECT DISTINCT other.object_id id FROM office_object office, office_object other WHERE (office.TYPE = 'office') AND (other.OBJECT_ID = 2) AND (SDO_RELATE(office.geometry, other.geometry, 'mask=COVERS') = 'TRUE')";

        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();

        if(rs.next()) {
            return true;
        }
        return false;
    }

    // SPATIAL FUNCTION 1
    public boolean validateGeometry(Connection conn) throws SQLException {
        if (id == -1){
            update(conn);
        }

        String sql = "SELECT type, SDO_GEOM.VALIDATE_GEOMETRY_WITH_CONTEXT(geometry, 0.1) valid "+
        "FROM OFFICE_OBJECT where object_id ="+id;
        PreparedStatement stmt = conn.prepareStatement(sql);
        ResultSet sr = stmt.executeQuery();

        if (sr.next()){
            if (sr.getBoolean("valid"))
                return true;
            else
                return false;
        }
        return false;
    }

    // SPATIAL FUNCTION 2
    // vracia zoznam id stolov ktore su susedne
    public List<Integer> neighbourTables(Connection conn, List<OfficeComponentObject> list) throws SQLException {
        if (id == -1)
            throw new RuntimeException("Object was not stored to DB");

        String sql  = "SELECT DISTINCT other.object_id id"+
            "FROM office_object office, office_object other"+
            "WHERE (office.type = 'table') AND"+
            "(other.object_id = ? ) AND"+
            "(SDO_RELATE(office.geometry, other.geometry, 'mask=ANYINTERACT') = 'FALSE')";

        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, id);
        ResultSet rs = stmt.executeQuery();

        List<Integer> ids = new ArrayList<>();
        while (rs.next()) {
            ids.add(rs.getInt("id"));
        }
        return ids;
    }

    // SPATIAL ANALYTICAL FUNCTIONS

    public double totalTableArea(Connection conn) throws SQLException {
        if (office_id == -1)
            throw new RuntimeException("Assign office first.");

        String sql = "SELECT SUM(SDO_GEOM.SDO_AREA(geometry, 1)) total_table_area FROM office_object"+
                " WHERE office_id = ? AND type = 'table'";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, office_id);
        ResultSet rs = stmt.executeQuery();

        if (rs.next()){
            return rs.getDouble("total_table_area");
        }
        return 0.0;
    }

    // can be called on any object from the office
    public double officePerimeter(Connection conn) throws SQLException {
        if (office_id == -1)
            throw new RuntimeException("Assign office first.");

        String sql = "SELECT SUM(SDO_GEOM.SDO_LENGTH(geometry, 1)) office_perimeter FROM office_object"+
                " WHERE office_id = ? AND type = 'office'";
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, office_id);
        ResultSet rs = stmt.executeQuery();

        if (rs.next()){
            return rs.getDouble("office_perimeter");
        }
        return 0.0;
    }
    public double distance(Connection conn, OfficeComponentObject obj) throws SQLException {
        if (office_id == -1 || obj.getId() == -1)
            throw new RuntimeException("Assign office first.");
        String sql = "SELECT SDO_GEOM.SDO_DISTANCE(this.geometry, other.geometry, 1) distance " +
                "FROM office_object this, office_object other WHERE " +
                "this.object_id = ? AND other.object_id = ? ";

        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setInt(1, this.office_id);
        stmt.setInt(2, obj.office_id);
        ResultSet rs = stmt.executeQuery();

        if (rs.next()){
            return rs.getDouble("distance");
        }
        return 0.0;
    }
    /* end of OfficeComponentObject */
}
