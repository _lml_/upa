package dbs.oracle_lab_multimedia.models;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Backend model. Reprezentuje databázovou entitu.
 */
public abstract class Model {
    protected int id = -1;
    protected boolean modified = false;

    /**
     * Set id for an object. Id is generated in DB, this method is only used for initialisation
     * or when id is retrieved from DB when object is being created.
     * This method should be used only from OfficeObjectsRepository and Model classes.
     *
     * @param   id   Id for object.
     * @see          dbs.oracle_lab_multimedia.repositories.OfficeObjectsRepository
     * @see          dbs.oracle_lab_multimedia.models.Model
     */
    public    void setId(int id){this.id = id;}
    /**
     * Get object id.
     * This method should be used only from OfficeObjectsRepository and Model classes.
     *
     * @return       Objects id.
     */
    public    int  getId(){return id;}
    /**
     * Set modified property if object was changed since it was initialized.
     * This method should be used only from OfficeObjectsRepository and Model classes.
     *
     * @see          dbs.oracle_lab_multimedia.repositories.OfficeObjectsRepository
     * @see          dbs.oracle_lab_multimedia.models.Model
     */
    public    void setModified(){modified=true;}
    /**
     * Unset modified property after object initialisation.
     * This method should be used only from OfficeObjectsRepository and Model classes.
     *
     * @see          dbs.oracle_lab_multimedia.repositories.OfficeObjectsRepository
     * @see          dbs.oracle_lab_multimedia.models.Model
     */
    public    void unsetModified(){modified=false;}

    /**
     * Updates DB when object was created or modified. When object was not
     * modified does nothing.
     * <p>
     * If object is being added to DB, id is generated in DB and set to the
     * object. If operation with DB fails, then throw exception.
     * <p>
     * OfficeComponentObject and PersonObject must have assigned office.
     * When office is not assigned throws RuntimeException.
     *
     * @param  conn  connection to database
     * @see          Connection
     * @see          OfficeComponentObject
     */
    public abstract void update(Connection conn) throws RuntimeException, SQLException;
   /**
    * Deletes object from DB. If object object isn't in DB then
    * does nothing.
    *
    * @param  conn  connection to database
    * @see          Connection
    */
    public abstract void delete(Connection conn) throws SQLException;

}
