package dbs.oracle_lab_multimedia.converters;

import dbs.oracle_lab_multimedia.models.OfficeComponentObject;
import dbs.oracle_lab_multimedia.views.PolygonView;
import dbs.oracle_lab_multimedia.views.PolylineView;
import dbs.oracle_lab_multimedia.views.View;
import javafx.collections.ObservableList;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Shape;
import oracle.spatial.geometry.JGeometry;

public class OfficeComponentConvertor {
    public static View toView(OfficeComponentObject model){
        View view = null;
        double[] points = model.getGeometry().getOrdinatesArray();
        String type = model.getType();
        result res;
        switch (type){
            case OfficeComponentObject.DOOR:
                res = jGeometry2Polygon(model, true);
                view = new PolygonView((Polygon) res.shape, model);
                view.setTranslateX(res.offsetX);
                view.setTranslateY(res.offsetY);
                res.shape.setFill(Color.DARKMAGENTA);
//                {
//                double x1 = points[0];
//                double y1 = points[1];
//                double x2 = points[2];
//                double y2 = points[3];
//                double translateX = (x2 - x1) / 2 + x1;
//                double translateY = (y2 - y1) / 2 + y1;
//                Polygon polygon = new Polygon();
//                polygon.getPoints().addAll(
//                        x1 - translateX, y1 - translateY,
//                        x2 - translateX, y1 - translateY,
//                        x2 - translateX, y2 - translateY,
//                        x1 - translateX, y2 - translateY
//                );
//                view = new PolygonView(polygon, model);
//                view.setTranslateX(translateX);
//                view.setTranslateY(translateY);
//                }
                break;
            case OfficeComponentObject.OFFICE:
                res = jGeometry2Polygon(model, true);
                view = new PolygonView((Polygon) res.shape, model);
                view.setTranslateX(res.offsetX);
                view.setTranslateY(res.offsetY);
                res.shape.setOpacity(0.3);
                res.shape.setFill(Color.BLUE);
                break;
            case OfficeComponentObject.TABLE:
                res = jGeometry2Polygon(model, true);
                view = new PolygonView((Polygon) res.shape, model);
                view.setTranslateX(res.offsetX);
                view.setTranslateY(res.offsetY);
                res.shape.setFill(Color.ORANGERED);
//                double x1 = points[0];
//                double y1 = points[1];
//                double x2 = points[2];
//                double y2 = points[3];
//                double translateX = (x2 - x1) / 2 + x1;
//                double translateY = (y2 - y1) / 2 + y1;
//                Polygon polygon = new Polygon();
//                polygon.getPoints().addAll(
//                        x1 - translateX, y1 - translateY,
//                        x2 - translateX, y1 - translateY,
//                        x2 - translateX, y2 - translateY,
//                        x1 - translateX, y2 - translateY
//                );
//                view = new PolygonView(polygon, model);
//                view.setTranslateX(translateX);
//                view.setTranslateY(translateY);
                break;
            case OfficeComponentObject.WALL:
                // podla mna translacia pri polyline nedava zmysel
                res = jGeometry2Polyline(model, true);
                view = new PolylineView((Polyline) res.shape, model);
                view.setTranslateX(res.offsetX);
                view.setTranslateY(res.offsetY);

                res.shape.setStroke(Color.DARKGRAY);
                break;
        }
        return view;
    }

    static double[] translateXY(OfficeComponentObject model, int npoints){
        double[] points = model.getGeometry().getOrdinatesArray();
        double translateX;
        double translateY;
        double minX;
        double maxX;
        double minY;
        double maxY;
        minX = points[0];
        maxX = points[0];
        minY = points[1];
        maxY = points[1];
        for (int i = 0; i< npoints; i++){
            if (i%2 == 0) { // X coordinates
                if (points[i] > maxX)
                    maxX = points[i];
                if (points[i] < minX)
                    minX = points[i];
            } else {
                if (points[i] > maxY)
                    maxY = points[i];
                if (points[i] < minY)
                    minY = points[i];

            }
        }
        // neviem ci som to pochopil spravne
        translateX = (maxX-minX)/2 + minX;
        translateY = (maxY-minY)/2 + minY;
        double[] translate = {translateX, translateY};
        return translate;

    }

    static result jGeometry2Polyline(OfficeComponentObject model, boolean translate)
    {
        if (model.getGeometry().getType() != JGeometry.GTYPE_CURVE){
            throw new RuntimeException("Object is not a polygon. Object type: "+model.getGeometry().getType());
        }
        double[] points = model.getGeometry().getOrdinatesArray();
        double[] translateOffset = {0.,0.};
        int npoints = model.getGeometry().getNumPoints() * 2;

        if (translate) {
            translateOffset = translateXY(model, npoints);
        }

        Polyline polyline = new Polyline();
        for (int i = 0; i< npoints; i++) {
            if (i%2 == 0) { // X coordinates
                polyline.getPoints().add(points[i] - translateOffset[0]);
            } else {
                polyline.getPoints().add(points[i] - translateOffset[1]);
            }
        }
        return new result(polyline, translateOffset[0], translateOffset[1]);
    }

    static result jGeometry2Polygon(OfficeComponentObject model, boolean translate) throws RuntimeException
    {
        if (model.getGeometry().getType() != JGeometry.GTYPE_POLYGON){
            throw new RuntimeException("Object is not a polygon. Object type: "+model.getGeometry().getType());
        }
        double[] points = model.getGeometry().getOrdinatesArray();
        double[] translateOffset = {0.,0.};
        int npoints = model.getGeometry().getNumPoints() * 2;

        if (translate) {
            translateOffset = translateXY(model, npoints);
        }

        Polygon polygon = new Polygon();
        for (int i = 0; i< npoints; i++) {
            if (i%2 == 0) {  // X coordinates
                polygon.getPoints().add(points[i] - translateOffset[0]);
            } else {
                polygon.getPoints().add(points[i] - translateOffset[1]);
            }
        }
        return new result(polygon, translateOffset[0], translateOffset[1]);
    }

    public static void updateModel(View view){
        if(view instanceof PolygonView){
            Polygon polygon = ((PolygonView) view).getPolygon();
            double offsetX = view.getTranslateX();
            double offsetY = view.getTranslateY();
            ObservableList<Double> viewPoints = polygon.getPoints();
            double[] points = new double[viewPoints.size()];
            for(int i = 0; i < viewPoints.size(); i += 2){
                points[i] = viewPoints.get(i) + offsetX;
                points[i + 1] = viewPoints.get(i + 1) + offsetY;
            }
            ((OfficeComponentObject)view.getModel()).setGeoPolygon(points);
            view.getModel().setModified();
        }else if(view instanceof PolylineView){
            Polyline polyline = ((PolylineView) view).getPolyline();
            double offsetX = view.getTranslateX();
            double offsetY = view.getTranslateY();
            ObservableList<Double> viewPoints = polyline.getPoints();
            double[] points = new double[viewPoints.size()];
            for(int i = 0; i < viewPoints.size(); i += 2){
                points[i] = viewPoints.get(i) + offsetX;
                points[i + 1] = viewPoints.get(i + 1) + offsetY;
            }
            ((OfficeComponentObject)view.getModel()).setGeoLine(points);
            view.getModel().setModified();
        }
    }

    static class result {
        Shape shape;
        double offsetX;
        double offsetY;

        public result(Shape shape, double offsetX, double offsetY) {
            this.shape = shape;
            this.offsetX = offsetX;
            this.offsetY = offsetY;
        }
    }
}
