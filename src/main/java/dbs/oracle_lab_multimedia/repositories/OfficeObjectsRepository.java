package dbs.oracle_lab_multimedia.repositories;

import oracle.jdbc.pool.OracleDataSource;
import oracle.jdbc.OracleResultSet;
import oracle.ord.im.OrdImage;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.*;

import oracle.spatial.geometry.JGeometry;
import dbs.oracle_lab_multimedia.models.OfficeComponentObject;
import dbs.oracle_lab_multimedia.models.OfficeObject;
import dbs.oracle_lab_multimedia.models.PersonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Repozitář pro všechny objekty v budovách.
 *
 * Poskytuje CRUD operace pro backend modely včetně výběru s filtrováním.
 */
public class OfficeObjectsRepository {
    public OfficeObjectsRepository(){}

    public void scriptRunner(Connection conn, String filename)
            throws SQLException, FileNotFoundException {
      File in = new File(filename);
      Scanner s = new Scanner(in);
      s.useDelimiter("/\\*[\\s\\S]*?\\*/|--[^\\r\\n]*|;");
      Statement st = null;

      try {
          st = conn.createStatement();
          while (s.hasNext()){
              String line = s.next().trim();
              System.out.println("Executing: "+ line);
              if (!line.isEmpty())
                  st.execute(line);
          }
      }
      finally
      {
          if (st != null)
          st.close();
      }
    }

    /**
     * Returns list of offices stored in database.
     *
     * @param  conn  connection to database
     * @see          Connection
     * @see          OfficeObject
     */
    public List<OfficeObject> getOffices(Connection conn) // vrati zoznam kancelarii
    {
        String sql = "Select office_id, company from office";
        List<OfficeObject> offices = new ArrayList<OfficeObject>();
        try {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                OfficeObject office = new OfficeObject();
                office.setOfficeId(rs.getInt(1));
                office.setCompany(rs.getString(2));

                office.unsetModified(); // set modified flag to false
                offices.add(office);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return offices;
    }
    /**
     * Returns list of objects retrieved from database, that are in an office passed as an argument.
     * OfficeObject must have a valid office_id.
     *
     * @param  conn  connection to database
     * @param  office an office we are interested in
     * @return list of geometric object in an office
     * @see          Connection
     * @see          OfficeObject
     * @see          OfficeComponentObject
     */
    public List<OfficeComponentObject> getObjects(Connection conn, OfficeObject office)
    {
        String sql = "Select object_id, type, geometry "+
                "from office_object where office_id = ?";
        if (office.getId() == -1){
            throw new RuntimeException("Office doesn't have assigned generated id. Run office.update()");
        }
        List<OfficeComponentObject> objects = new ArrayList<OfficeComponentObject>();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, office.getId());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                OfficeComponentObject obj = new OfficeComponentObject();
                obj.assignOffice(office);
                obj.setId(rs.getInt(1));
                obj.setType(rs.getString(2));
                final byte[] geo = rs.getBytes(3);
                obj.setGeometry(JGeometry.load(geo));

                // set modified flag to false
                obj.unsetModified();
                objects.add(obj);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return objects;
    }
    /**
     * Returns list of people retrieved from database that are from office passed as an argument.
     * OfficeObject must have a valid office_id.
     *
     * @param  conn  connection to database
     * @param  office an office we are interested in
     * @return list of people from one office
     * @see          Connection
     * @see          OfficeObject
     * @see          PersonObject
     */
    public List<PersonObject>  getPeople(Connection conn, OfficeObject office){
        String sql = "Select person_id, name, birth, desk_id, photo "+
                "from person where office_id = ?";
        if (office.getId() == -1){
            throw new RuntimeException("Office doesn't have assigned generated id. Run office.update()");
        }
        List<PersonObject> people = new ArrayList<PersonObject>();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, office.getId());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                PersonObject obj = new PersonObject();
                obj.assignOffice(office);
                obj.setId(rs.getInt(1));
                obj.setName(rs.getString(2));
                obj.setDate(rs.getDate(3));
                obj.setTable(rs.getInt(4));
                if (rs.wasNull()){
                    obj.setTable(-1);
                }
                final OracleResultSet ors = (OracleResultSet) rs;
                obj.setPhoto((OrdImage) ors.getORAData(5, OrdImage.getORADataFactory()));
                // set modified flag to false
                obj.unsetModified();
                people.add(obj);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return people;
    }

    /**
     * Creates connection to DB.
     * DB addres is hardcoded: gort.fit.vutbr.cz:1521/orclpdb
     * If creating connection fails returns null.
     *
     * @param  login    user login
     * @param  password user password
     * @return Connection to DB
     * @see          Connection
     * @see          OfficeObject
     */
    public Connection createConnection(String login, String password){
        Connection conn = null;
        try {
            OracleDataSource ods = new OracleDataSource();
            ods.setURL("jdbc:oracle:thin:@//gort.fit.vutbr.cz:1521/orclpdb");
            ods.setUser(login);
            ods.setPassword(password);
            conn = ods.getConnection();
            return conn;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
