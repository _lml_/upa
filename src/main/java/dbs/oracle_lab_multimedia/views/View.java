package dbs.oracle_lab_multimedia.views;

import dbs.oracle_lab_multimedia.models.Model;
import javafx.scene.Group;

public abstract class View extends Group {
    private double scale = 1.0;
    Model model;

    public void resize(double scale){
        this.scale = scale;
    }

    public double getScale() {
        return scale;
    }

    public Model getModel() {
        return model;
    }

    public void select(){

    }

    public void deselect(){

    }
}
