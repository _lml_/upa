package dbs.oracle_lab_multimedia.views;

import dbs.oracle_lab_multimedia.models.OfficeComponentObject;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polygon;

public class PolygonView extends View{
    private Polygon polygon;
    private Double[] originalPoints;
    private Paint baseColor;

    public PolygonView(Polygon polygon, OfficeComponentObject model) {
        this.polygon = polygon;
        this.model = model;

        getChildren().add(polygon);

        originalPoints = new Double[polygon.getPoints().size()];
        for(int i = 0; i < polygon.getPoints().size(); ++i){
            originalPoints[i] = polygon.getPoints().get(i);
        }
    }

    @Override
    public void resize(double scale) {
        super.resize(scale);
        polygon.getPoints().clear();
        for(double d : originalPoints){
            polygon.getPoints().add(d * scale);
        }
    }

    public Polygon getPolygon() {
        return polygon;
    }

    @Override
    public void select(){
        if(baseColor == null)
            baseColor = polygon.getFill();
        polygon.setFill(Color.RED);
    }

    @Override
    public void deselect(){
        if(baseColor != null)
            polygon.setFill(baseColor);
    }
}
