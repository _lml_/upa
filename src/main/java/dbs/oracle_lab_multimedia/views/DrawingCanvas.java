package dbs.oracle_lab_multimedia.views;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;

public class DrawingCanvas extends Pane {
    private View selected;
    private View previouslySelected;
    private Point2D dragPoint;
    private BooleanProperty isSelected = new SimpleBooleanProperty();
    private boolean readonly = true;

    public void add(View object) {
        this.getChildren().add(object);

        object.setOnMouseDragged(e -> {
            if(readonly)
                return;
            Point2D p = object.localToParent(new Point2D(e.getX(), e.getY()));
            object.setTranslateX(p.getX() - dragPoint.getX());
            object.setTranslateY(p.getY() - dragPoint.getY());
        });
        object.setOnMousePressed(e -> {
            dragPoint = new Point2D(e.getX(), e.getY());
            setSelected(object);
            object.toFront();
        });
    }

    public void addOffice(View office){
        getChildren().add(office);
    }

    public void remove(View object) {
        this.getChildren().remove(object);
        if(object == selected){
            setSelected(null);
        }
    }

    public void select(View view){
        setSelected(view);
    }

    public void clear(){
        setSelected(null);
        getChildren().clear();
    }

    public boolean isObjectSelected(){
        if(selected != null)
            return true;
        return false;
    }

    public View getSelected() {
        return selected;
    }

    public View getPreviouslySelected() {
        return previouslySelected;
    }

    public boolean isReadonly() {
        return readonly;
    }

    public void setReadonly(boolean readonly) {
        this.readonly = readonly;
    }

    public final boolean getIsSelected(){
        return isSelected.get();
    }

    public final void setIsSelected(){

    }

    /***
     * Urcuje, zda je vybrany nejaky objekt
     *
     * Pokud je nejaky objekt vybrany a vybere se jiny, dojde ke zmene na false a hned zpatky na true
     */
    public BooleanProperty isSelectedProperty(){
        return isSelected;
    }

    private void setSelected(View selected) {
        this.previouslySelected = this.selected;
        this.selected = null;
        isSelected.setValue(false);
        if(selected == null)
            return;
        this.selected = selected;
        isSelected.setValue(true);
    }
}
