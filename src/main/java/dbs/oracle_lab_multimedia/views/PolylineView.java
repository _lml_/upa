package dbs.oracle_lab_multimedia.views;

import dbs.oracle_lab_multimedia.models.OfficeComponentObject;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Polyline;

public class PolylineView extends View{
    private Polyline polyline;
    private Double[] originalPoints;
    private Paint baseColor;

    public PolylineView(Polyline polyline, OfficeComponentObject model) {
        this.polyline = polyline;
        this.model = model;

        polyline.setStrokeWidth(8);
        getChildren().add(polyline);

        originalPoints = new Double[polyline.getPoints().size()];
        for(int i = 0; i < polyline.getPoints().size(); ++i){
            originalPoints[i] = polyline.getPoints().get(i);
        }
    }

    public Polyline getPolyline() {
        return polyline;
    }

    @Override
    public void resize(double scale) {
        super.resize(scale);
        polyline.getPoints().clear();
        for(double d : originalPoints){
            polyline.getPoints().add(d * scale);
        }
    }

    @Override
    public void select(){
        if(baseColor == null)
            baseColor = polyline.getStroke();
        polyline.setStroke(Color.RED);
    }

    @Override
    public void deselect(){
        if(baseColor != null)
            polyline.setStroke(baseColor);
    }
}
