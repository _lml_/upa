package dbs.oracle_lab_multimedia;

import dbs.oracle_lab_multimedia.models.OfficeComponentObject;
import dbs.oracle_lab_multimedia.models.OfficeObject;
import dbs.oracle_lab_multimedia.models.PersonObject;
import dbs.oracle_lab_multimedia.repositories.OfficeObjectsRepository;
import javafx.application.Application;
import javafx.stage.Stage;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import static javafx.application.Platform.exit;

/**
 * Hello world!
 */
public class App extends Application{

    public static void main(String[] args) throws Exception {

//        OfficeObjectsRepository repo = new OfficeObjectsRepository();
//        String login = System.getProperty("login");
//        String passwd = System.getProperty("password");
//        Connection conn = repo.createConnection(login, passwd);
//        repo.scriptRunner(conn, "drop_tables.sql");
//        repo.scriptRunner(conn, "create_tables.sql");
//        System.out.println("Sql script has been run.");
//        List<OfficeObject> office_list = repo.getOffices(conn);
//        OfficeObject office = null;
//        if (office_list.size() > 0)
//            office = office_list.get(0);
//        else
//            office = OfficeObject.createSampleAndStore2DB(conn);
//
//        List<OfficeComponentObject> objects;
//        objects = repo.getObjects(conn, office);
//        if (objects.size() == 0) {
//            objects.add(OfficeComponentObject.createSampleAndStore2DB(conn, office, OfficeComponentObject.OFFICE));
//            objects.add(OfficeComponentObject.createSampleAndStore2DB(conn, office, OfficeComponentObject.TABLE));
//            objects.add(OfficeComponentObject.createSampleAndStore2DB(conn, office, OfficeComponentObject.WALL));
//            objects.add(OfficeComponentObject.createSampleAndStore2DB(conn, office, OfficeComponentObject.DOOR));
//        }
//
//        List<PersonObject> people;
//        people = repo.getPeople(conn, office);
//        if (people.size() == 0) {
//            for (int i = 1; i <= 5; i++) {
//                people.add(PersonObject.createSampleAndStore2DB(conn, office, i));
//            }
//        }

        launch(args);
        //pozdejc odstranit
        if(args.length < 20) {
            exit();
            return;
        }


        /**
         * *
         * To set System properties, run the Java VM with the following at
         * its command line: ... -Dlogin=LOGIN_TO_ORACLE_DB
         * -Dpassword=PASSWORD_TO_ORACLE_DB ... or set the project
         * properties (in NetBeans: File / Project Properties / Run / VM
         * Options)
         */
//        }

    }

    @Override
    public void start(Stage stage) throws Exception {
        System.out.println("app started");
        new UiStartupManager(stage);
    }
}
