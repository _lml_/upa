package dbs.oracle_lab_multimedia.utils;

import javafx.scene.control.Alert;

public class AlertUtil {
    public static void showGeneralError(String msg){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Error occured");
        alert.setContentText(msg);
        alert.showAndWait();
    }

    public static void showDbGeneralError(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText("Error occured");
        alert.setContentText("Database operation failed.");
        alert.showAndWait();
    }
}
