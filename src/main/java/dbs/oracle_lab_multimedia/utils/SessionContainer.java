package dbs.oracle_lab_multimedia.utils;

import dbs.oracle_lab_multimedia.models.OfficeObject;
import dbs.oracle_lab_multimedia.models.PersonObject;

public class SessionContainer {
    private OfficeObject officeObject;
    private PersonObject personObject;

    public OfficeObject getOfficeObject() {
        return officeObject;
    }

    public void setOfficeObject(OfficeObject officeObject) {
        this.officeObject = officeObject;
    }


    public PersonObject getPersonObject() { return personObject; }

    public void setPersonObject(PersonObject personObject) { this.personObject = personObject; }
}
