package dbs.oracle_lab_multimedia.utils;

import java.sql.Connection;

public class ConnectionContainer {
    private Connection connection;

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
