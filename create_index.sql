DROP INDEX office_geometry_idx;
CREATE INDEX office_geometry_idx ON office_object(geometry) INDEXTYPE IS MDSYS.SPATIAL_INDEX_V2;
COMMIT;
