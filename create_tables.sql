/* for future me:
sqlplus xslavk01/nWFY4di7@gort.fit.vutbr.cz:1521/orclpdb
SQL> @<path to this script>/product-init.sql

z nejakeho dovodu intellij hlasi stale chybu pri spustani
*/

select count(*)
from all_objects
where object_type in ('TABLE','VIEW')
and object_name = 'PERSON';

select count(*)
from all_objects
where object_type in ('TABLE','VIEW')
and object_name = 'OFFICE_OBJECT';

select count(*)
from all_objects
where object_type in ('TABLE','VIEW')
and object_name = 'OFFICE';

select count(*)
from all_objects
where object_type in ('TABLE','VIEW')
and object_name = 'REGISTRATION';

-- toto je jedna kancelaria/poschodie + nejake metadata
-- DROP TABLE OFFICE;
CREATE TABLE OFFICE
(office_id INTEGER GENERATED ALWAYS AS IDENTITY,
 company VARCHAR ( 255 ),
 PRIMARY KEY ( office_id ) );
-- objekty ako stoly steny miestnosti okna (to je type)
-- office_id je fk na office v ktorom je objekt
-- reprezentuje ju trieda OfficeComponentObject
CREATE TABLE OFFICE_OBJECT
     (object_id INTEGER GENERATED ALWAYS AS IDENTITY,
     type VARCHAR(255),
     office_id INTEGER,
     geometry SDO_GEOMETRY,
     FOREIGN KEY ( office_id ) REFERENCES office( office_id ) ON DELETE CASCADE,
     PRIMARY KEY ( object_id ));

-- ludia budu mat priradeny office ked sa vlozia do systemu
-- ak desk_id nie je null tak maju priradeny stol
CREATE TABLE PERSON
    (person_id INTEGER GENERATED ALWAYS AS IDENTITY,
	name VARCHAR(255),
	birth DATE,
	desk_id INTEGER,
	office_id INTEGER,
	photo ORDSYS.ORDImage,
	photo_si ORDSYS.SI_StillImage,
	photo_ac ORDSYS.SI_AverageColor,
	photo_ch ORDSYS.SI_ColorHistogram,
	photo_pc ORDSYS.SI_PositionalColor,
	photo_tx ORDSYS.SI_Texture,
    PRIMARY KEY( person_id ),
    FOREIGN KEY( desk_id ) REFERENCES office_object( object_id ) ON DELETE CASCADE,
    FOREIGN KEY( office_id ) REFERENCES office( office_id ) ON DELETE CASCADE );

-- trigger computing photo metadata
--CREATE OR REPLACE TRIGGER photos_generateFeatures
--  AFTER INSERT OR UPDATE OF photo ON PERSON
--  FOR EACH ROW
--DECLARE
--  si ORDSYS.SI_StillImage;
--BEGIN
--  si := new SI_StillImage(:NEW.photo.getContent());
--  UPDATE PERSON p SET photo_si = si,
--    photo_ac = SI_AverageColor(si),
--    photo_ch = SI_ColorHistogram(si),
--    photo_pc = SI_PositionalColor(si),
--    photo_tx = SI_Texture(si)
--    WHERE p.id = :NEW.id;
--END;

--SELECT * FROM tabs;
